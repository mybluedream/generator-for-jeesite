/*****************************************************************
 *仿佛兮若轻云之蔽月，飘飘兮若流风之回雪
 *@filename DBExcelContext.java
 *@author WYY
 *@date 2013年11月24日
 *@copyright (c) 2013  wyyft@163.com All rights reserved.
 *****************************************************************/
package com.featherlike.feather.generator.excel;

import java.util.List;
import java.util.Map;

import com.featherlike.feather.generator.config.ConfigProperties;
import com.featherlike.feather.generator.config.Constant;
import com.featherlike.feather.generator.entity.Column;
import com.featherlike.feather.generator.entity.Table;

public final class ExcelContext {
    private static ExcelContext instance;

    public static ExcelContext getInstance() {
        if (null == instance) {
            instance = new ExcelContext(ConfigProperties.getExcelParser());
        }
        return instance;
    }

    private ExcelMapper excelMapper;

    /**
     * 根据配置文件实例化解析工具，默认使用POI
     * @param excelParser
     */
    private ExcelContext(String excelParser) {
        if (Constant.JXL.equalsIgnoreCase(excelParser)) {
            this.excelMapper = new ExcelMapperJXL();
        } else if (Constant.POI.equalsIgnoreCase(excelParser)) {
            this.excelMapper = new ExcelMapperPOI();
        } else {
            this.excelMapper = new ExcelMapperPOI();
        }

    }

    public Map<Table, List<Column>> getTableMapFromExcel(String inputPath) {
        return excelMapper.createTableMap(inputPath);
    }

}

/*****************************************************************
 *
 *@filename DBExcelMapperPOI.java
 *@author EverWang
 *@date 2013-9-26
 *@copyright (c) 2012-2022  wyyft@163.com All rights reserved.
 *
 *****************************************************************/
package com.featherlike.feather.generator.excel;

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.featherlike.framework.common.util.IOUtil;

/**
 * 使用POI读取数据库的Excel转换成tableMap，替换原来的jxl，实现统一 TODO
 * @author EverWang
 * @Description
 */
public class ExcelMapperPOI extends ExcelMapper {
    private FileInputStream fis;
    private HSSFWorkbook hssfWorkbook;
    private HSSFSheet hssfSheet;

    public HSSFSheet getSheet(int index) {
        return hssfWorkbook.getSheetAt(index);
    }

    @Override
    String getSheetName() {
        return hssfSheet.getSheetName().toLowerCase();
    }

    @Override
    int getNumberOfSheets() {
        return hssfWorkbook.getNumberOfSheets() + 1;
    }

    @Override
    int getNumOfRows() {
        return hssfSheet.getLastRowNum();
    }

    @Override
    String getCellValue(int column, int row) {
        return hssfSheet.getRow(row).getCell(column).getStringCellValue()
                .trim();
    }

    @Override
    void init(String inputPath) throws Exception {
        fis = new FileInputStream(inputPath);
        hssfWorkbook = new HSSFWorkbook(fis);

    }

    @Override
    void distroy() {
        IOUtil.closeQuietly(fis);
    }

    @Override
    void initSheet(int index) {
        hssfSheet = hssfWorkbook.getSheetAt(index);

    }
}
